from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets, filters, generics
from menu_app import serializers, models, permissions

from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.serializers import api_settings

class CreateUserView(generics.CreateAPIView):
  serializer_class = serializers.UserSerializer

class CreateTokenView(ObtainAuthToken):
  serializer_class = serializers.AuthTokenSerializer
  renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES

class UserViewSet(viewsets.ModelViewSet):
  """Crear y atualizar perfiles de usuario"""
  serializer_class = serializers.UserSerializer
  queryset = models.User.objects.all()
  permission_classes = (permissions.UpdateOwnProfile,)
  authentication_classes = (TokenAuthentication,)
  filter_backends = (filters.SearchFilter,)
  search_fields = ('name', 'email')

class UserLoginApiView(ObtainAuthToken):
  renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES

class UserMenuViewSet(viewsets.ModelViewSet):
  authentication_classes = (TokenAuthentication,)
  serializer_class = serializers.UserMenuSerializer
  queryset = models.UserMenu.objects.all()
  permission_classes = (permissions.UpdateOwnMenu, IsAuthenticated)

  def perform_create(self, serializer):
    serializer.save(user_profile = self.request.user)