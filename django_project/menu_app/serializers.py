from rest_framework import serializers
from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import ugettext_lazy as _
from menu_app import models

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'name', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'min_length': 6,
                'style': {'input_type': 'password'}
            }
        }

    def create(self, validated_data):
        user = get_user_model().objects.create_user(
            email = validated_data['email'],
            password = validated_data['password'],
            name=validated_data['name'],
        )
        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)
        return super().update(instance, validated_data)

class AuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField(
        style = {'input_type': 'password'},
        trim_whitespace = False
    )

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(
            request = self.context.get('request'),
            username = email,
            password = password
        )
        if not user:
            msg = _('No se puede autenticar con las credenciales ingresadas')
            raise serializers.ValidationError(msg, code = 'authorization')

        attrs['user'] = user
        return attrs

class MenuSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Menu
        fields = ('id', 'date', 'description', 'option')

class UserMenuSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.UserMenu
        fields = ('id', 'user', 'customization')
        extra_kwargs = {
            'user': {'read_only': True}
        }

