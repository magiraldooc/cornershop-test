from django.urls import path, include
from rest_framework.routers import DefaultRouter
from menu_app import views

router = DefaultRouter()
router.register('profile', views.UserViewSet)
router.register('menu', views.UserMenuViewSet)

urlpatterns = [
  path('login/', views.UserLoginApiView.as_view()),
  path('', include(router.urls)),

  path('user/create/', views.CreateUserView.as_view(), name = 'user_create'),
  path('user/token/', views.CreateTokenView.as_view(), name = 'user_token')
]