from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse

# Create your tests here.

class AdminTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.admin_user = get_user_model().objects.create_superuser(
            email = 'admin@test.com',
            password = '123456',
            name='Test'
        )
        self.client.force_login(self.admin_user)

        self.user = get_user_model().objects.create_user(
            email = 'user@test.com',
            password = '123456',
            name='Test'
        )

    def test_user_listed(self):

        url = reverse('admin:menu_app_user_changelist')
        res = self.client.get(url)

        self.assertContains(res, self.user.email)
        self.assertContains(res, self.user.name)

    def test_user_change_page(self):

        url = reverse('admin:menu_app_user_change', args = [self.user.id])
        res = self.client.get(url)

        self.assertEqual(res.status_code, 200)

    def test_create_user_page(self):

        url = reverse('admin:menu_app_user_add')
        res = self.client.get(url)

        self.assertEqual(res.status_code, 200)