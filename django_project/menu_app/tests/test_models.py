from django.test import TestCase
from django.contrib.auth import get_user_model

# Create your tests here.

class ModelTest(TestCase):
    def test_create_user_with_email_successful(self):
        email = 'test@test.com'
        name = 'Test'
        password = '123456'

        user = get_user_model().objects.create_user(
            email = email,
            name = name,
            password = password
        )

        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_create_user_invalid_email(self):
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, '123456')