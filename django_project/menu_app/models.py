from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.conf import settings
# Create your models here.

"""Manager para permisos de usuario"""
class UserManager(BaseUserManager):

  """"Crear un perfil de usuario"""
  def create_user(self, email, password = None, **extra_fields):
    if not email:
      raise ValueError('El usuario debe tener correo')
    email = self.normalize_email(email)
    user = self.model(email = email, **extra_fields)
    user.set_password(password)
    user.save(using=self._db)
    return user

  def create_superuser(self, email, password, **extra_fields):
    user = self.create_user(email, password, **extra_fields)
    user.is_superuser = True
    user.is_staff = True
    user.save(using=self._db)
    return user


"""Modelo base de datos para usuario en el sistema"""
class User(AbstractBaseUser, PermissionsMixin):
  email = models.EmailField(max_length=255, unique=True)
  name = models.CharField(max_length=255)
  is_active = models.BooleanField(default=True)
  is_staff = models.BooleanField(default=False)

  objects = UserManager()

  USERNAME_FIELD = 'email'
  REQUIRED_FIELDS = ['name']

  """Obtener nombre completo"""
  def get_full_name(self):
    return self.name

  """Obtener nombre corto"""
  def get_short_name(self):
    return self.name

  """Retorna una cadena representando el usuario"""
  def __str__(self):
      return self.email

class Menu():
  date = models.DateField(auto_now_add=True)
  description = models.TextField()
  option = models.IntegerField(max_length=2)

  def create_menu(self, date, description, option):
    # if not email:
    # raise ValueError('El usuario debe tener correo')
    # email = self.normalize_email(email)
    menu = self.model(date=date, description=description, option=option)
    menu.save(using=self._db)
    return menu

class UserMenu(models.Model):
  user = models.ForeignKey(
    settings.AUTH_USER_MODEL,
    on_delete = models.CASCADE
  )
  customization = models.TextField()

  def __str__(self):
    return self.customization